from settings import HIVE_METASTORE


def get_hoodie_config(
    hudi_table_name: str,
    record_key: str,
    precombine: str,
    partition_field: str,
    write_operation: str = "UPSERT",
    table_type: str = "COPY_ON_WRITE",
) -> dict:
    """
    return config of hoodie
    Args:
        topic (str): topic name

    Returns:
        dict: hoodie options
    """
    hoodie_options = {
        "hoodie.table.name": hudi_table_name,
        "hoodie.metadata.enable": "true",
        "hoodie.table.type": table_type,
        "hoodie.datasource.write.table.type": table_type,
        "hoodie.datasource.write.operation": write_operation,
        "hoodie.datasource.write.recordkey.field": record_key,
        "hoodie.datasource.write.partitionpath.field": partition_field,
        "hoodie.datasource.write.table.name": hudi_table_name,
        "hoodie.datasource.write.precombine.field": precombine,
        "hoodie.clean.automatic": "true",
        "hoodie.cleaner.policy": "KEEP_LATEST_FILE_VERSIONS",
        "hoodie.cleaner.fileversions.retained": 10,
        # "hoodie.compact.inline.max.delta.commits": 3, MERGE ON READ
        "hoodie.datasource.write.hive_style_partitioning": "true",
        "hoodie.upsert.shuffle.parallelism": "2",
        "hoodie.insert.shuffle.parallelism": "2",
        "hoodie.datasource.hive_sync.enable": "true",
        "hoodie.datasource.hive_sync.mode": "hms",
        "hoodie.datasource.hive_sync.database": "default",
        "hoodie.datasource.hive_sync.table": hudi_table_name,
        "hoodie.datasource.hive_sync.partition_fields": partition_field,
        "hoodie.datasource.hive_sync.partition_extractor_class": "org.apache.hudi.hive.MultiPartKeysValueExtractor",
        "hoodie.datasource.hive_sync.metastore.uris": HIVE_METASTORE,
    }
    return hoodie_options
