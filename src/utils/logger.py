import logging
import os

import coloredlogs

LOGGING_LEVEL = os.getenv("LOGGING_LEVEL", "DEBUG")
LEVEL = {
    "INFO": logging.INFO,
    "DEBUG": logging.DEBUG,
    "WARNING": logging.WARNING,
    "ERROR": logging.ERROR,
}

logger = logging.getLogger("UX Backend")
log_format = "[%(asctime)s|%(threadName)10s|%(levelname)7s|%(filename)s:%(lineno)03d] %(message)s"
formatter = logging.Formatter(log_format)
coloredlogs.install(
    logger=logger, milliseconds=True, fmt=log_format, level=LEVEL[LOGGING_LEVEL]
)
