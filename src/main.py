import argparse
from typing import Optional

from hoodie_config import get_hoodie_config
from pyspark.sql import SparkSession
from pyspark.sql.dataframe import DataFrame
from pyspark.sql.functions import col, explode
from pyspark.sql.types import StructType
from settings import AWS_BUCKET, MINIO_ACCESS_KEY, MINIO_SECRET_KEY, MINIO_SERVER_HOST
from utils.api_utils import ApiFetcher
from utils.logger import logger


class HudiSink:
    """
    Ingest data from topic ux_data to MINIO
    """

    def __init__(
        self, hudi_table_name, record_key, precombine, partition_fields
    ) -> None:
        self.spark = (
            SparkSession.builder.appName("spark application")
            .config(
                "spark.jars.packages",
                "org.apache.spark:spark-avro_2.12:3.1.1,"
                "org.apache.hadoop:hadoop-aws:3.1.1,"
                "com.amazonaws:aws-java-sdk:1.11.271,"
                "org.apache.hudi:hudi-spark3.1-bundle_2.12:0.13.0",
            )
            .config("spark.driver.memory", "4g")
            .config("spark.sql.codegen.wholeStage", "false")
            .config(
                "spark.sql.extensions",
                "org.apache.spark.sql.hudi.HoodieSparkSessionExtension",
            )
            .config("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
            .config(
                "spark.hadoop.fs.s3a.impl", "org.apache.hadoop.fs.s3a.S3AFileSystem"
            )
            .config("spark.hadoop.fs.s3a.access.key", MINIO_ACCESS_KEY)
            .config("spark.hadoop.fs.s3a.secret.key", MINIO_SECRET_KEY)
            .config("spark.hadoop.fs.s3a.endpoint", MINIO_SERVER_HOST)
            .config("spark.hadoop.fs.s3a.path.style.access", "true")
            .config("spark.hadoop.fs.s3a.connection.ssl.enabled", "false")
            .config(
                "spark.hadoop.fs.s3a.aws.credentials.provider",
                "org.apache.hadoop.fs.s3a.SimpleAWSCredentialsProvider",
            )
            .config("spark.hadoop.fs.s3a.connection.maximum", "1000")
            .getOrCreate()
        )
        self.hudi_table_name = hudi_table_name
        self.record_key = record_key
        self.precombine = precombine
        self.partition_fields = partition_fields
        self._spark_sc = self.spark.sparkContext
        self._spark_sc.setLogLevel("ERROR")

    def json_to_spark_dataframe(self, data: str):
        res_rdd = self.spark.sparkContext.parallelize(data)
        df = self.spark.read.json(res_rdd)
        return df

    @staticmethod
    def extract_data_fields_str(fields: str) -> list:
        return [field_name.strip() for field_name in fields.split(",")]

    @staticmethod
    def get_arguments():
        parser = argparse.ArgumentParser()
        parser.add_argument("--hudiTableName", help="Hudi table name")
        parser.add_argument("--recordKey", help="Primary key of Hudi table")
        parser.add_argument("--precombine", help="Precombine")
        parser.add_argument("--partitionField", help="Partition Field")
        parser.add_argument("--explodeField", help="Unwind array")
        args = parser.parse_args()
        return vars(args)

    @staticmethod
    def get_all_columns_from_schema(source_schema):
        branches = []

        def inner_get(schema, ancestor=None):
            if ancestor is None:
                ancestor = []
            for field in schema.fields:
                branch_path = ancestor + [field.name]
                if isinstance(field.dataType, StructType):
                    inner_get(field.dataType, branch_path)
                else:
                    branches.append(branch_path)

        inner_get(source_schema)
        return branches

    @staticmethod
    def collapsing_structured_columns(
        _all_columns, nested_fields: Optional[str] = None
    ):
        _columns_to_select = []
        if nested_fields:
            nested_fields = [
                nested_field.strip() for nested_field in nested_fields.split(",")
            ]
        for column_collection in _all_columns:
            if nested_fields:
                nested_field_checked = False
                for nested_field in nested_fields:
                    if nested_field.strip() in column_collection:
                        nested_field_checked = True
                        continue
            if nested_fields:
                if nested_field_checked is True:
                    continue

            _select_column_collection = [
                "`%s`" % list_item for list_item in column_collection
            ]

            if len(column_collection) > 1:
                _columns_to_select.append(
                    col(".".join(_select_column_collection)).alias(
                        "_".join(column_collection)
                    )
                )
            else:
                _columns_to_select.append(col(_select_column_collection[0]))
        return _columns_to_select

    def list_all_columns(self, source_schema, nested_fields: Optional[str] = None):
        columns = self.get_all_columns_from_schema(source_schema=source_schema)
        all_columns = self.collapsing_structured_columns(
            columns, nested_fields=nested_fields
        )
        return all_columns

    def tranform_dataframe(
        self,
        url: str,
        data_field: Optional[str] = None,
        selected_fields: Optional[str] = None,
        explode_columns: Optional[str] = None,
        nested_fields: Optional[str] = None,
    ) -> DataFrame:
        api_fetcher = ApiFetcher()
        data = api_fetcher.fetch_data(url=url, data_field=data_field)
        df = self.json_to_spark_dataframe(data=data)

        if not selected_fields:
            try:
                selected_column = self.list_all_columns(
                    source_schema=df.schema, nested_fields=nested_fields
                )

                if explode_columns:
                    logger.info(f"Processing explode column: {explode_columns}")
                    df = df.withColumn(
                        explode_columns, explode(col=explode_columns.strip())
                    )

                    selected_column = self.list_all_columns(
                        source_schema=df.schema, nested_fields=nested_fields
                    )

                if nested_fields:
                    logger.info(f"Processing nested field: {nested_fields}")

                    # spilit nested field str into list and rename column
                    nested_fields = self.extract_data_fields_str(nested_fields)
                    nested_fields = [
                        col(nested_field).alias(nested_field.replace(".", "_"))
                        for nested_field in nested_fields
                    ]

                    selected_column = [*selected_column, *nested_fields]

                df = df.select(selected_column)

                logger.info("Finally DataFrame")
                df.show(n=5)
                logger.info(f"Number of record: {df.count()}")
                df.printSchema()
                return df
            except Exception as e:
                logger.error(e)
        else:
            try:
                selected_fields = self.extract_data_fields_str(selected_fields)
                selected_fields = [
                    col(selected_field).alias(selected_field.replace(".", "_"))
                    for selected_field in selected_fields
                ]
                df = df.select(selected_fields)

                selected_column = self.list_all_columns(
                    source_schema=df.schema, nested_fields=nested_fields
                )
                if explode_columns:
                    logger.info(f"Processing explode column: {explode_columns}")
                    if explode_columns not in selected_fields:
                        logger.error(
                            f"{explode_columns} is not selected in {selected_fields}"
                        )
                        return
                    df = df.withColumn(
                        explode_columns, explode(col=explode_columns.strip())
                    )
                    selected_column = self.list_all_columns(
                        source_schema=df.schema, nested_fields=nested_fields
                    )

                if nested_fields:
                    logger.info(f"Processing nested column: {nested_fields}")
                    selected_column = [
                        *selected_column,
                        *self.extract_data_fields_str(nested_fields),
                    ]
                df = df.select(selected_column)

                logger.info("Finally DataFrame")
                df.show(n=5)
                print(f"Number of record: {df.count()}")
                df.printSchema()
                return df
            except Exception as e:
                logger.error(f"Error: {e}")

    def save_format_hudi(self, df: DataFrame) -> None:
        logger.info(f"Number of record: {df.count()}")
        hoodie_options = get_hoodie_config(
            hudi_table_name=self.hudi_table_name,
            record_key=self.record_key,
            precombine=self.precombine,
            partition_field=self.partition_fields,
        )

        logger.info(f"Saving {self.hudi_table_name} ...")
        try:
            (
                df.write.mode("append")
                .format("hudi")
                .options(**hoodie_options)
                .save(f"s3a://{AWS_BUCKET}/{self.hudi_table_name}")
            )
        except Exception as e:
            logger.error(e)

        print("Save done!!")


if __name__ == "__main__":
    # url = "https://dummyjson.com/products"
    url = "https://dummyjson.com/users"
    # url = "https://console-smartux.vnpt.vn/serving/heatmap-v2?app_key=07fb4f18e9d03542d1071efba75db74287762e81&start_time=1689526800000&end_time=1690131599999&event_type=click&screen_size_type=Desktop1366&limit=9999&domain=cdha-sannhivpc.vnpthis.vn&view=%2Flogin"
    # url = "https://smartux-dev.icenter.ai/serving/mobile/heatmap?app_key=8384d17cbca947ae8c4c72564c5e5f9b987b9608&limit=9999&event_type=touch&start_time=1689526800000&end_time=1690131599999&device_os=iOS&group_name=iPhone%2012%2F13%2F14%20Pro%20Max(19.5%3A9)"
    hudi_sink = HudiSink(
        hudi_table_name="api_products",
        record_key="id",
        precombine="price",
        partition_fields="brand",
    )

    df = hudi_sink.tranform_dataframe(
        url=url,
        data_field="users",
        selected_fields="id, lastName, address.coordinates, bank, company, username, hair",
        explode_columns="",
        nested_fields="address_coordinates, bank, company",
    )

    # hudi_sink.save_format_hudi(df=df)
