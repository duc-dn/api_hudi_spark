RUN_ENV = "SERVER"
# RUN_ENV = None

if RUN_ENV:
    MINIO_ACCESS_KEY = "minioadmin"
    MINIO_SECRET_KEY = "minioadmin"
    AWS_BUCKET = "hudi-api"

    # minio server in docker
    MINIO_SERVER_HOST = "http://10.159.19.121:9000/"

    # kafka server
    # KAFKA_BOOTSTRAP_SERVERS = "10.163.16.26:9092"
    HIVE_METASTORE = "thrift://10.159.19.102:9083"
else:
    MINIO_ACCESS_KEY = "minioadmin"
    MINIO_SECRET_KEY = "minioadmin"
    AWS_BUCKET = "datalake"

    # minio server in docker
    MINIO_SERVER_HOST = "http://minio:9000"

    # kafka server
    # KAFKA_BOOTSTRAP_SERVERS = "10.163.16.26:9092"

    HIVE_METASTORE = "thrift://hive-metastore:9083"
