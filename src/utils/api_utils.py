from typing import Optional

import requests
from requests.exceptions import HTTPError
from utils.logger import logger


class ApiFetcher:
    def __init__(self):
        pass

    def fetch_data(
        self,
        url: str,
        data_field: Optional[str] = None,
        token: Optional[str] = None,
        **kwargs,
    ):
        try:
            headers = {"Authorization": f"Bearer {token}"}

            response = requests.get(url, headers=headers, params=kwargs)
            response.raise_for_status()

            if response.status_code == 200:
                logger.info("Get data from API sucessfully!!")
                data = response.json()
                return data[data_field] if data_field else data
        except HTTPError as http_err:
            logger.error(f"HTTP error ocurred: {http_err}")
        except requests.exceptions.RequestException as e:
            logger.error(f"Error fetching data from {url}: {e}")
        except Exception as err:
            logger.error(f"Error when get data: {err}")


if __name__ == "__main__":
    url = "https://min-api.cryptocompare.com/data/histoday?fsym=BTC&tsym=JPY&limit=30&aggregate=1&e=CCCAGG"
    api = ApiFetcher(url)
    data = api.fetch_data()
    print(data)
