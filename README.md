## Ingest data from API, save format Hudi with SPARK
#### Spark submit
```
spark-submit --master local \
--packages org.apache.hudi:hudi-spark3.1-bundle_2.12:0.13.0 \
--conf 'spark.serializer=org.apache.spark.serializer.KryoSerializer' \
--conf 'spark.sql.extensions=org.apache.spark.sql.hudi.HoodieSparkSessionExtension' \
main.py \
--hudiTableName="check" \
--recordKey="id" \
--precombine="price" \
--partitionField="" --explodeField=""
```
---
```
HudiSink().save_to_minio(
        data=data,
        hudi_table_name="test_book",
        record_key="id,images",
        precombine="price",
        data_field=None,
        partition_field="brand",
        explode_column="images"
    )
```
---
#### Explain some important arguments (**REQUIREMENT** in Hudi Table)
- record_key field is unique in Hudi Table (if in api don't unique field, we can use more 2 fieds to make record key)
- precombine (if record key is duplicate, hudi will use this field to indentify record which is written in Hudi table)
- partition_field (Optional, default: "") (it is used to partition data, if you want to use it, you can set partition_field is "")
- explode_column (Optional, default: None) (if data of api have array field, you can explode it. You just pass field which you want to explode. Opposite, you just set explode_column = "")
- data_field: (Optional, default: None) (This field is field which contain actual data in API). Such as:
```
{
    "Response": "Success",
    "Type": 100,
    "Aggregated": false,
    "TimeTo": 1689811200,
    "TimeFrom": 1687219200,
    "FirstValueInArray": true,
    "ConversionType": {
        "type": "direct",
        "conversionSymbol": ""
    },
    "Data": [
        {
        "time": 1687219200,
        "high": 4005239.59,
        "low": 3773578.76,
        "open": 3809106.2,
        "volumefrom": 2488.57,
        "volumeto": 9667249841.81,
        "close": 3989400.39,
        "conversionType": "direct",
        "conversionSymbol": ""
        },
        {
        "time": 1687305600,
        "high": 4360735.99,
        "low": 3985096.98,
        "open": 3989400.39,
        "volumefrom": 4138.91,
        "volumeto": 17194442133.51,
        "close": 4243602.4,
        "conversionType": "direct",
        "conversionSymbol": ""
        }
    ]
}

In this case, data_field is Data
